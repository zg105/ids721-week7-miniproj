# Week 7 Mini Project

This project deals data processing with vector database

## Start
1. Create a new Rust project.

2. Add the required dependencies to `Cargo.toml` file.

3. start the qdrant service.
![](pic/1.png)

4. In `main.rs`, I implement the data ingestion and query functionality.

5. After ingesting the data, we can run `cargo run` to run query:
![](pic/2.png)

