use anyhow::{Result};
use qdrant_client::prelude::*;
use qdrant_client::qdrant::{
    CreateCollection, PointStruct, SearchPoints, Condition, Filter, VectorParams, VectorsConfig
};
use qdrant_client::qdrant::vectors_config::Config;
use serde_json::json;
use std::convert::TryInto;

#[tokio::main]
async fn main() -> Result<()> {
    let client = initialize_qdrant_client().await?;

    let collection_name = "product_catalog";
    create_collection(&client, collection_name).await?;
    ingest_products(&client, collection_name).await?;
    query_products(&client, collection_name).await?;

    Ok(())
}

async fn initialize_qdrant_client() -> Result<QdrantClient> {
    let config = QdrantClientConfig::from_url("http://localhost:6334");
    let client = QdrantClient::new(Some(config))?;
    Ok(client)
}

async fn create_collection(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let _ = client.delete_collection(collection_name).await;

    client.create_collection(&CreateCollection {
        collection_name: collection_name.into(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 4, // Assuming a 4-dimensional vector for simplicity
                distance: Distance::Euclid.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    }).await?;

    Ok(())
}

async fn ingest_products(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let products = vec![
        PointStruct::new(
            1,
            vec![0.2, 0.5, 0.8, 0.3],
            json!({"product": "Laptop"}).try_into().unwrap(),
        ),
        PointStruct::new(
            2,
            vec![0.6, 0.7, 0.2, 0.4],
            json!({"product": "Smartphone"}).try_into().unwrap(),
        ),
        PointStruct::new(
            3,
            vec![0.9, 0.3, 0.6, 0.7],
            json!({"product": "Tablet"}).try_into().unwrap(),
        ),
        // Add more products as needed
    ];

    client.upsert_points_blocking(collection_name, None, products, None).await.unwrap();

    Ok(())
}

async fn query_products(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![0.6, 0.6, 0.6, 0.6],
            filter: Some(Filter::all([Condition::matches(
                "product",
                "Laptop".to_string(),
            )])),
            limit: 2,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await
        .unwrap();

    println!("Search Results After Filter:");
    for (index, point) in search_result.result.iter().enumerate() {
        let formatted_payload = serde_json::to_string_pretty(&point.payload).unwrap_or_default();
        println!("Point {}: \n - Payload: {}\n - Score: {}\n", index + 1, formatted_payload, point.score);
    }

    Ok(())
}
